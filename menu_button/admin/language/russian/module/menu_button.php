<?php
// Heading
$_['heading_title']       = 'Кнопка Меню';

// Text
$_['text_module']         = 'Модули';
$_['text_success']        = 'Модуль Кнопка Меню успешно изменен!';
$_['text_edit']           = 'Редактировать модуль Кнопка Меню';
$_['text_enabled']        = 'Включен';
$_['text_disabled']       = 'Отключен';

// Entry
$_['entry_name']          = 'Имя модуля';
$_['entry_width']         = 'Ширина';
$_['entry_height']        = 'Высота';
$_['entry_banner']        = 'Баннер';
$_['entry_status']        = 'Статус';

// Error
$_['error_permission']    = 'Внимание: Недостаточно прав для изменения модуля Кнопка Меню!';
$_['error_name']          = 'Имя модуля должно состоять от 3 до 64 символов!';
$_['error_width']         = 'Укажите ширину!';
$_['error_height']        = 'Укажите высоту!';
		
// Button
$_['button_save']         = 'Сохранить';
$_['button_cancel']       = 'Отмена';