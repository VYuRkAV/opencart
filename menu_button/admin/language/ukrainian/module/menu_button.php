<?php
// Heading
$_['heading_title']       = 'Кнопка Меню';

// Text
$_['text_module']         = 'Модулі';
$_['text_success']        = 'Ви успішно змінили модуль Кнопка Меню!';
$_['text_edit']           = 'Змінити модуль Кнопка Меню';
$_['text_enabled']        = 'Ввімкнено';
$_['text_disabled']       = 'Вимкнено';

// Entry
$_['entry_name']          = 'Назва модуля';
$_['entry_width']         = 'Ширина';
$_['entry_height']        = 'Висота';
$_['entry_banner']        = 'Банер';
$_['entry_status']        = 'Статус';

// Error
$_['error_permission']    = 'Увага: У Вас немає доступу до зміни модулю Кнопка Меню!';
$_['error_name']          = 'Назва модуля повинна бути не менш 3 та не більше 64 символів!';
$_['error_width']         = 'Вкажіть ширину!';
$_['error_height']        = 'Вкажіть висоту!';
		
// Button
$_['button_save']         = 'Зберегти ';
$_['button_cancel']       = 'Скасувати';