<?php if ($categories) { ?>
<div id="menu-button<?php echo $module; ?>" class="panel panel-default">
  <?php foreach ($banners as $banner) { ?>
  <button type="button" class="btn btn-default btn-block pull-left" data-toggle="collapse" data-target="#menu-collapse">
    <?php if ($banner['image']) { ?>
      <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-rounded" /><span class="hidden-xs">&nbsp;&nbsp;&nbsp;<?php echo $banner['title']; ?></span>
    <?php } else { ?>
      <span class="hidden-xs"><?php echo $banner['title']; ?></span>
    <?php } ?>
  </button>
    <div id="menu-collapse" class="collapse">
      <ul class="nav nav-stacked">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown-submenu"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
          <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
            <ul class="dropdown-menu">
              <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
              <?php } ?>
            </ul>
          <?php } ?>
        </li>
          <?php } else { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
          <?php } ?>
        <?php } ?>
      </ul>
    </div>
  <?php } ?>
</div>
<?php } ?>